# require 'rails_helper'
# require 'ostruct'

# describe SmsRuApi do
#   let(:api_key){Rails.application.secrets.sms_ru_api_key}
#   it "should raise error if no api key is given" do
#     expect{SmsRuApi.new}.to raise_error(SmsRuApi::WrongApiKeyError)
#   end

#   it "should not raise error if api matches right" do
#     expect{SmsRuApi.new(api_key)}.not_to raise_error
#   end

#   describe "send method" do
#     let (:sms_ru){SmsRuApi.new(api_key, true)}

#     it "should have send method" do
#       expect(sms_ru).to respond_to(:send_message)
#     end

#     it "should return no contact given error" do
#       result = sms_ru.send_message('message', '')
#       expect(result[:status]).to eq(SmsRuApi::STATUSES[:error])
#     end

#     it "should return wrong contact given error" do
#       result = sms_ru.send_message('message', 'wrong')
#       expect(result[:status]).to eq(SmsRuApi::STATUSES[:error])
#     end

#     it "should send test message and return message status" do
#       result = sms_ru.send_message('message', '9894460820')
#       expect(result[:status]).to eq(SmsRuApi::STATUSES[:queue])
#     end
#   end

#   describe "status method" do
#     let (:sms_ru){SmsRuApi.new(api_key, true)}

#     def stub_with_code(code)
#       allow(Curl).to receive(:get).and_return(OpenStruct.new(body: code))
#     end

#     it "should get message status" do
#       stub_with_code('103')
#       result = sms_ru.status('201351-888569')
#       expect(result[:status]).to eq(SmsRuApi::STATUSES[:success])
#     end

#     it "should return error message if response body if empty" do
#       # empty body means the id does not exists
#       stub_with_code('')
#       result = sms_ru.status('00000-00000')
#       expect(result[:status]).to eq(SmsRuApi::STATUSES[:error])
#     end

#     it "should return queue status if it's from 100 to 102" do
#       ['100', '101', '102'].each do |code|
#         stub_with_code(code)
#         result = sms_ru.status('201351-888569')
#         result[:status] == SmsRuApi::STATUSES[:queue]
#       end
#     end

#     it "should return error if message does not have been sent" do
#       stub_with_code('105')
#       result = sms_ru.status('201351-888569')
#       result[:status] == SmsRuApi::STATUSES[:error]
#     end
#   end
# end
