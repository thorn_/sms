require 'rails_helper'

describe ContactExtractor do
  let(:user)     { FactoryGirl.create(:user) }
  let(:contact1) { FactoryGirl.create(:contact, user: user) }
  let(:contact2) { FactoryGirl.create(:contact, user: user) }
  let(:contact3) { FactoryGirl.create(:contact, user: user) }
  let(:group)    { FactoryGirl.create(:group, user: user) }

  it "should extract contacts" do
    contacts = ContactExtractor.extract({group_ids: [group.id], contact_ids: [contact1.id, contact2.id, contact3.id], numbers: ["1234"]}, user)
    expect(contacts.length).to eq(4)
  end

  context "parse numbers method" do
    it "should return valid numbers from array or strings" do
      numbers = ContactExtractor.parse_numbers ["928763", "23423424",nil, ["123", "456"]]
      expect(numbers).to eq(["928763", "23423424", "123", "456"])
    end
  end

  context "get files from files" do
    let(:user){FactoryGirl.create(:user)}
    let(:file_name){ "/files/example.xls" }
    let(:file){ fixture_file_upload(file_name, "application/vnd.ms-excel") }

    it "should take file and read whole shit of it" do
      result = ContactExtractor.extract_from_spreadsheet(file, user)
      expect(result.length).to eq(20)
      expect(result.first.number).to eq("9030169668")
      expect(result.last.number).to  eq("9034080388")
    end

    it "should extract number if it is right" do
      numbers = [
        "+79261234567",
        "89261234567",
        "79261234567",
        "+7 926 123 45 67",
        "8(926)123-45-67",
        "9261234567",
        "79261234567",
        "89261234567",
        "8-926-123-45-67",
        "8 927 1234 234",
        "8 927 12 12 888",
        "8 927 12 555 12",
        "8 927 123 8 123"
      ]
      wrong_numbers = [
        "(495)1234567",
        "(495) 123 45 67",
        "123-45-67",
        "bred123",
        "123holy123",
        "crap123145"
      ]
      numbers.each do |number|
        expect(ContactExtractor.parse_number(number)).not_to eq(false)
      end
      wrong_numbers.each do |number|
        expect(ContactExtractor.parse_number(number)).to eq(false)
      end
    end
  end

end
