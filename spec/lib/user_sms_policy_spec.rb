# require 'rails_helper'
require_relative '../../app/lib/user_sms_policy'
require "ostruct"

describe UserSmsPolicy do
  let(:user) {OpenStruct.new(sms_amount: 10)}
  let(:message) {OpenStruct.new(sms_count: 1, contacts: [1,2,3])}

  it "should say 1 sms to send" do
    policy = UserSmsPolicy.new(user, message)
    expect(policy.sms_to_send).to eq(3)
  end

  it "should say yes" do
    policy = UserSmsPolicy.new(user, message)
    expect(policy.can_send_message?).to eq(true)
  end

  it "should say no if user has not enough balance" do
    user.sms_amount = 0
    policy = UserSmsPolicy.new(user, message)
    expect(policy.can_send_message?).to eq(false)
  end

end
