require 'rails_helper'

describe MessageCreator do
  let(:user)  { FactoryGirl.create(:user) }
  let(:group) { FactoryGirl.create(:group, user: user) }
  let(:contact1) { FactoryGirl.create(:contact, user: user) }
  let(:contact2) { FactoryGirl.create(:contact, user: user) }
  let(:attr) {
    {
      group_ids: [group.id],
      contact_ids: [contact1.id, contact2.id],
      numbers: ["9887654321"],
      message:{
        text: "text"
      }
    }
  }

  it "should return false if there is no contacts" do
    message = MessageCreator.create(attr.merge(contact_ids: nil, group_ids: nil, numbers: nil), user)
    expect(message).to eq(false)
  end

  it "should return message with right number if message amount" do
    message = MessageCreator.create(attr, user)
    expect(message.sms_to_send).to eq(3)
  end
end
