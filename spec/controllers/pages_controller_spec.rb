require 'rails_helper'

describe PagesController do
  render_views

  before(:each) do
    @user = FactoryGirl.create(:user)
    sign_in @user
  end
  describe "GET 'show'" do
    it "should render template by it's id" do
      get :show, id: :test
      expect(response.body).to match(/test pages template/)
    end
  end

  describe "application controller before actions" do
    it "should set locale from user" do
      @user.update_attributes(language: "en")
      get :show, id: :test
      expect(I18n.locale).to eq(:en)
    end

    it "should set locale from http header" do
      request.env['HTTP_ACCEPT_LANGUAGE'] = 'en'
      get :show, id: :test
      expect(I18n.locale).to eq(:en)
    end
  end
end
