# -*- encoding: utf-8 -*-
require 'rails_helper'

describe Admin::UsersController do

  before(:each) do
    @user = FactoryGirl.create(:user)
    sign_in @user
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      expect(response).to be_success
    end
  end

  describe "DELETE 'destroy'" do
    it "should destroy given user" do
      expect do
        delete :destroy, id: @user.id
        expect(response).to redirect_to(admin_users_path)
      end.to change(User, :count).by(-1)
    end

    it "should have a flash message" do
      delete :destroy, id: @user.id
      expect(flash[:success]).to match(/removed/i)
    end
  end

  describe "PUT 'update'" do
    before(:each) do
      put :update, id: @user.id, user: {role: "administrator", email: FactoryGirl.generate(:email)}
    end

    it "should update users credentials" do
      old_email = @user.email
      @user.reload
      expect(@user.role).to eq("administrator")
      expect(@user.email).not_to eq(old_email)
    end

    it "should redirecto to users path" do
      expect(response).to redirect_to(admin_users_path)
    end

    it "should have appropriate flash message" do
      expect(flash[:success]).to match(/success/i)
    end
  end

  describe "POST 'create'" do
    let(:attributes) {  {role: "administrator", email: FactoryGirl.generate(:email), password: 'password', password_confirmation: 'password'} }
    it "should create a record if all attributes are valid" do
      expect do
        post :create, user:attributes
      end.to change{User.count}.by(1)
    end

    it "should render edit template" do
      expect do
        post :create, user:attributes.merge(email: nil)
      end.not_to change{User.count}
    end
  end

  describe "GET 'new'" do
    it "should be ok" do
      get :new
      expect(response).to be_ok
    end
  end
end
