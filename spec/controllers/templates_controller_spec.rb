require 'rails_helper'

describe TemplatesController do
  render_views
  before(:each) do
    @user = FactoryGirl.create(:user)
    sign_in @user
  end
  describe "GET 'show'" do
    it "should render template without layout" do
      get :show, controller_name: :test, id: :test
      expect(response.body).to eq("test template\n")
    end
  end
end
