require 'rails_helper'
require 'rspec/rails'
describe Api::V1::MessagesController do
  render_views
  let(:user){FactoryGirl.create(:user)}
  let(:message){FactoryGirl.create(:message, user: user)}

  describe "GET 'index'" do
    it "should deny request if there is no access token" do
      get :index
      expect(response).not_to be_ok
    end

    it "should be okay if there is api key of user" do
      get :index, access_token: user.access_token, format: :json
      expect(response).to be_ok
    end

    it "should be okay" do
      get :index, access_token: user.access_token, format: :json
      expect(response).to be_ok
    end

    it "should fetch all user's messages" do
      message = FactoryGirl.create(:message, user: user)
      get :index, access_token: user.access_token, format: :json
      expect(assigns(:messages)).to include(message)
    end
  end

  describe "POST 'create'" do
    before(:each) do
      allow(MessageCreator).to receive(:create).and_return(FactoryGirl.create(:message))
    end

    it "shoud have right response" do
      post :create, access_token: user.access_token, format: :json
      expect(response).to be_ok
    end

    it "should call message creator with params" do
      expect(MessageCreator).to receive(:create)
      post :create, access_token: user.access_token, format: :json
    end

    it "should return error if there message is not created" do
      allow(MessageCreator).to receive(:create).and_return(false)
      post :create, access_token: user.access_token, format: :json
      expect(response.body).to match(/error/)
    end
  end

  describe "GET 'send'" do
    # let(:attr){{text: "text message", url: "http://example.com", access_token: user.access_token, testing: true}}

    # before(:each) do
    #   @contact1 = FactoryGirl.create(:contact, user: user)
    #   @contact2 = FactoryGirl.create(:contact, user: user)
    # end

    # it "should create record" do
    #   expect do
    #     get :send_message, attr, format: :json
    #   end.to change{Message.count}.by(1)
    # end

    # it "should render error if attributes are not correct" do
    #   get :send_message, attr.merge(text: nil), format: :json
    #   expect(response.body).to match(/error/i)
    # end

    # it "should also create 2 contact message records" do
    #   expect do
    #     get :send_message, attr, format: :json
    #   end.to change{ContactMessage.count}.by(2)
    # end

    # it "should assign job id to created contact messages" do
    #   get :send_message, attr, format: :json
    #   expect(assigns(:message).contact_messages.count).to be > 0
    #   assigns(:message).contact_messages.each do |pm|
    #     expect(pm.jid).not_to eq(nil)
    #   end
    # end

    # it "should render error if user has no contacts" do
    #   user = FactoryGirl.create(:user)
    #   get :send_message, attr.merge(access_token: user.access_token), format: :json
    #   expect(response.body).to match(/error/i)
    # end

    # it "should just not save record if testing parameter is true" do
    #   get :send_message, attr.merge(test: "true"), format: :json
    #   expect(response.body).to match(/ok/i)
    # end

    # it "should render error when invalid params and testing parameter is true" do
    #   get :send_message, attr.merge(text: "", test: "true"), format: :json
    #   expect(response.body).to match(/error/i)
    # end
  end

  describe "POST 'set_status'" do
    it "should render 100 in any time" do
      post :set_status
      expect(response.body).to eq("100")
    end

    it "should set status to some messages" do
      message1 = FactoryGirl.create(:contact_message, service_id: 'uid1')
      message2 = FactoryGirl.create(:contact_message, service_id: 'uid2')
      post :set_status, "data"=>{"0"=>"sms_status\nuid1\n103", "1"=>"sms_status\nuid2\n103"}
      message1.reload
      message2.reload
      expect(message1.status).to eq('success')
      expect(message2.status).to eq('success')
    end
  end

  describe "GET 'show'" do
    let(:message){FactoryGirl.create(:message, user: user)}
    let(:wrong_user_message){FactoryGirl.create(:message)}

    it "should be ok" do
      get :show, id: message.id, access_token: user.access_token, format: :json
      expect(response).to be_ok
    end

    it "should fetch given message" do
      get :show, id: message.id, access_token: user.access_token, format: :json
      expect(assigns(:message)).to eq(message)
    end

    it "should return 404 error if there is no such message" do
      get :show, id: wrong_user_message.id, access_token: user.access_token, format: :json
      expect(response).not_to be_ok
      expect(response.body).to match(/error/)
    end

    it "should also get contact messages if there are some" do
      get :show, id: message.id, access_token: user.access_token, format: :json
      expect(assigns(:contact_messages)).not_to be_nil
    end
  end

  describe "DELETE 'destroy'" do
    let(:message) { FactoryGirl.create(:message, user: user) }
    it "fetches given message of user and deletes it" do
      message
      expect do
        delete :destroy, id: message.id, access_token: user.access_token, format: :json
        expect(assigns(:message)).to eq(message)
      end.to change{Message.count}.by(-1)
    end
  end
end
