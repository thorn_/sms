require 'rails_helper'

describe Api::V1::GroupsController do
  let(:user){FactoryGirl.create(:user)}
  let(:attr){{name: "group name"}}
  before(:each) {user.main_group.destroy}
  describe "GET 'index'" do
    it "should be okay" do
      get :index, access_token: user.access_token, format: :json
      expect(response).to be_ok
    end

    it "should fetch all the user's groups" do
      group1 = FactoryGirl.create(:group, user: user)
      group2 = FactoryGirl.create(:group)
      get :index, access_token: user.access_token, format: :json
      expect(assigns(:groups)).to include(group1)
      expect(assigns(:groups)).not_to include(group2)
    end
  end

  describe "POST 'create'" do
    it "should create a new record" do
      expect do
        post :create, group: attr, access_token: user.access_token, format: :json
      end.to change{Group.count}.by(1)
    end

    it "should create a new record" do
      expect do
        post :create, group: attr.merge(name: nil), access_token: user.access_token, format: :json
        expect(response).not_to be_ok
      end.not_to change{Group.count}
    end
  end

  describe "DELETE 'destroy'" do
    it "should delete group" do
      group = FactoryGirl.create(:group, user: user)
      expect do
        delete :destroy, id: group.id, access_token: user.access_token, format: :json
      end.to change{Group.count}.by(-1)
    end

    it "should return error" do
      group = FactoryGirl.create(:group)
      expect do
        delete :destroy, id: group.id, access_token: user.access_token, format: :json
      end.not_to change{Group.count}
    end

    it "should not touch the group if group is not editable" do
      group = FactoryGirl.create(:group, user: user, editable: false)
      expect do
        delete :destroy, id: group.id, access_token: user.access_token, format: :json
        expect(response).not_to be_ok
      end.not_to change{Group.count}
    end
  end

  describe "POST 'clear'" do
    let(:group) { FactoryGirl.create(:group, user: user) }
    it "should fetch appropriate group" do
      post :clear, id: group.id, access_token: user.access_token, format: :json
      expect(assigns(:group)).to eq(group)
    end

    it "should render error if group is not found" do
      post :clear, id: -1, access_token: user.access_token, format: :json
      expect(response).not_to be_ok
      expect(response.body).to match(/error/)
    end
  end

  describe "PATCH 'update'" do
    let(:group){ FactoryGirl.create(:group, user: user) }

    it "should update attributes of group" do
      expect do
        patch :update, id: group.id, group: {name: 'new_name'}, format: :json, access_token: user.access_token
        group.reload
      end.to change{group.name}.to('new_name')
    end

    it "should response with error if attributes are invalid" do
      expect do
        patch :update, id: group.id, group: {name: nil}, format: :json, access_token: user.access_token
        expect(response).not_to be_ok
        group.reload
      end.not_to change{group.name}
    end

    it "should not touch the group if group is not editable" do
      group = FactoryGirl.create(:group, user: user, editable: false)
      expect do
        patch :update, id: group.id, group: {name: "brand new name"}, format: :json, access_token: user.access_token
        expect(response).not_to be_ok
        group.reload
      end.not_to change{group.name}
    end
  end

  describe "GET 'show'" do
    let(:group){ FactoryGirl.create(:group, user: user)}

    it "should fetch appropriate group and be ok" do
      get :show, id: group.id, access_token: user.access_token, format: :json
      expect(assigns(:group)).to eql(group)
      expect(response).to be_ok
    end

  end
end
