require 'rails_helper'
include ActionDispatch::TestProcess

describe Api::V1::AssetsController do
  let(:user){FactoryGirl.create(:user)}
  let(:asset){FactoryGirl.create(:asset, user: user)}
  before(:each) do
    sign_in user
  end

  describe "GET 'index'" do
    it "should be okay" do
      get :index, access_token: user.access_token, format: :json
      expect(response).to be_ok
    end

    it "should get all assets of current user" do
      get :index, access_token: user.access_token, format: :json
      expect(assigns(:assets)).to include(asset)
    end

  end

  describe "DELETE 'destroy'" do
    it "should delete asset from user" do
      delete :destroy, id: asset.id, access_token: user.access_token, format: :json
      expect(response).to be_ok
    end
  end

  describe "POST 'create'" do
    let(:file_name){ "/files/example.xls" }
    let(:file){ fixture_file_upload(file_name, "application/vnd.ms-excel") }
    let(:attr){ {file: file} }

    it "should create a new asset" do
      expect do
        post :create, access_token: user.access_token, asset: attr, format: :json
      end.to change{Asset.count}.by(1)
    end

    it "should render some errors if there is not enough params" do
        post :create, access_token: user.access_token, asset: {name: "me"}, format: :json
        expect(response).not_to be_ok
        expect(response.body).to match(/errors/)
    end
  end
end
