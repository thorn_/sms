require 'rails_helper'

describe Api::V1::ContactsController do
  let(:user){FactoryGirl.create(:user)}

  describe "POST 'create'" do
    let(:attr){{number: "9894460820"}}
    it "should description" do
      expect do
        post :create, contact: attr, access_token: user.access_token, format: :json
      end.to change{Contact.count}.by(1)
    end

    it "should add contact to group if group id parameter is present" do
      group = FactoryGirl.create(:group, user: user)
      post :create, contact: attr,group_ids: group.id, access_token: user.access_token, format: :json
      expect(assigns(:contact).group_ids).to include(group.id)
    end

    it "should render error if there is no number" do
      post :create, contact: attr.merge(number: nil), access_token: user.access_token, format: :json
      expect(response).not_to be_ok
      expect(response.body).to match(/error/i)
    end
  end

  describe "GET 'add'" do
    let(:attr){ {number: "9894460820"} }

    it "should create a new record" do
      expect do
        get :add, attr.merge(access_token: user.access_token), format: :json
      end.to change{Contact.count}.by(1)
    end

    it "should render error if there is no number" do
      get :add, {access_token: user.access_token}, format: :json
      expect(response).not_to be_ok
      expect(response.body).to match(/error/i)
    end
  end

  describe "GET 'index'" do
    it "should get user's contacts" do
      contact = FactoryGirl.create(:contact, user: user)
      get :index, access_token: user.access_token, format: :json
      expect(assigns(:contacts)).to include(contact)
    end
  end

  describe "DELETE 'destroy'" do
    before(:each) do
      @group = FactoryGirl.create(:group, user: user, editable: true)
      @contact = FactoryGirl.create(:contact, user: user)
      @contact.group_ids = [@group.id]
      @contact.save!
    end

    it "should just delete contact from group is parameter is given" do
      expect do
        delete :destroy, id: @contact.id, from_group: @group.id, access_token: user.access_token, format: :json
      end.to change{ContactGroup.count}.by(-1)
    end

    it "should not change contact count in that way" do
      expect do
        delete :destroy, id: @contact.id, from_group: @group.id, access_token: user.access_token, format: :json
      end.not_to change{Contact.count}
    end

    it "should change contact hidden to false if from_group belongs to editable group" do
      @group.editable = false
      @group.save
      expect do
        delete :destroy, id: @contact.id, from_group: @group.id, access_token: user.access_token, format: :json
        @contact.reload
      end.to change{@contact.hidden}.from(false).to(true)
    end

    it "should find record by id" do
      delete :destroy, id: @contact.id, access_token: user.access_token, format: :json
      expect(assigns(:contact)).to eq(@contact)
    end

    it "should set hidden attribute to true" do
      expect do
        delete :destroy, id: @contact.id, access_token: user.access_token, format: :json
        @contact.reload
      end.to change{@contact.hidden}.from(false).to(true)
    end

  end

  describe "GET 'del'" do
    before(:each){ @contact = FactoryGirl.create(:contact, user: user) }

    it "should find record by number" do
      get :del, number: @contact.number, access_token: user.access_token
      expect(assigns(:contact)).to eq(@contact)
    end

    it "should mark record as hidden" do
      expect do
        get :del, number: @contact.number, access_token: user.access_token
        @contact.reload
      end.to change{@contact.hidden}.to(true)
    end

    it "should render error if there is no such record" do
      get :del, number: -1, access_token: user.access_token
      expect(response.body).to match(/error/i)
    end
  end

  describe "PATCH 'update'" do
    let(:attr){{user_id: user.id, name: "new contact name", number: FactoryGirl.generate(:number)}}
    it "should update record if attributes are valid" do
      contact = FactoryGirl.create(:contact, name: "name", user: user)
      expect do
        patch :update, id: contact.id, contact: attr, access_token: user.access_token, format: :json
        contact.reload
      end.to change{contact.name}.to('new contact name')
    end

    it "should add contact to group if group_id parameter is present" do
      group = FactoryGirl.create(:group, user: user)
      contact = FactoryGirl.create(:contact, name: "name", user: user)
      patch :update, id: contact.id, contact: attr, group_ids: [group.id], access_token: user.access_token, format: :json
      expect(assigns(:contact).group_ids).to include(group.id)
    end


    it "should not update record if attributes are invalid" do
      contact = FactoryGirl.create(:contact, name: "name", user: user)
      expect do
        patch :update, id: contact.id, contact: attr.merge(number: nil), access_token: user.access_token, format: :json
        expect(response).not_to be_ok
        contact.reload
      end.not_to change{contact.number}
    end

    it "should render not okay if there is no such contact in user's contact list" do
      contact = FactoryGirl.create(:contact, name: "name")
      expect do
        patch :update, id: contact.id, contact: attr, access_token: user.access_token, format: :json
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe "GET 'show'" do
    it "should fetch appropriate record" do
      contact = FactoryGirl.create(:contact, user: user)
      get :show, id: contact.id, access_token: user.access_token, format: :json
      expect(assigns(:contact)).to eq(contact)
    end

    it "should tell 404 if there is no such record" do
      contact = FactoryGirl.create(:contact)
      expect do
        get :show, id: contact.id, access_token: user.access_token, format: :json
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
