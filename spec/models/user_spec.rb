# --- encoding: utf-8 ---
require 'rails_helper'

describe User do
  let(:attr){ {email: FactoryGirl.generate(:email), password: "password", password_confirmation: "password", sms_amount: 10} }

  it "should create user with valid attributes" do
    expect do
      User.create!(attr)
    end.to change{User.count}.by(1)
  end

  it "should fill in access token after record creation" do
    user = User.create!(attr)
    expect(user.access_token).not_to be_empty
  end

  describe "validations" do
    it "should have many contact" do
      expect(User.new).to respond_to(:contacts)
    end

    it "should have many messages" do
      expect(User.new).to respond_to(:messages)
    end

    it "should have many groups" do
      expect(User.new).to respond_to(:groups)
    end

    it "should have many assets" do
      expect(User.new).to respond_to(:assets)
    end
  end

  context "authorization" do
    it "should have user roles" do
      expect(User.create(attr).role_symbols).to eq([:user])
    end

    it "should return readable names for roels" do
      expect(User.create(attr).user_role).to eq('User')
    end
  end

  context "default group" do
    it "should create main group after user creation" do
      expect do
        User.create(attr)
      end.to change{Group.count}.by(1)
    end
    it "should return user's default group" do
      user = User.create(attr)
      expect(user.main_group.name).to eq("All contacts")
    end
  end

  context "methods" do
    let(:user){FactoryGirl.create(:user, sms_amount: 10)}
    it "should charge user by given amount of sms" do
      user.charge(10)
      user.reload
      expect(user.sms_amount).to eq(0)
    end

    it "should not charge if user has less sms than is charging" do
      expect{user.charge(11)}.to raise_error(User::NotEnoughSms)
    end
  end
end
