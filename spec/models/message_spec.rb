require 'rails_helper'

describe Message do
  let(:user){FactoryGirl.create(:user, sms_amount: 1)}
  let(:attr) { {name: "message name", text: "message text", url: "http://example.com", user_id: user.id} }

  it "should create message with right values" do
    expect do
      Message.create(attr)
    end.to change{Message.count}.by(1)
  end

  describe "relations" do
    it "should belong to user" do
      expect(Message.new).to respond_to(:user)
    end

    it "should have meny contact messages" do
      expect(Message.new).to respond_to(:contact_messages)
    end

    it "should have many contacts" do
      expect(Message.new).to respond_to(:contacts)
    end
  end

  describe "validation" do
    it "should validate for user id" do
      expect(Message.new(attr.merge(user_id: nil))).not_to be_valid
    end

    it "should validate text" do
      expect(Message.new(attr.merge(text: nil))).not_to be_valid
    end

    it "should not create message with long text" do
      expect(Message.new(attr.merge(text: "s"*1531))).not_to be_valid
    end
  end

  describe "sms count method should return sms count based on message contacts and text length" do
    it "should contain 1 message with 69 symbols and cyrillic" do
      message = FactoryGirl.create(:message, text: "1"*68 + "А")
      expect(message.sms_count).to eq(1)
    end

    it "should contain 2 messages with 70 symbols and cyrillic" do
      message = FactoryGirl.create(:message, text: "1"*70 + "А")
      expect(message.sms_count).to eq(2)
    end

    it "should contain 1 message with 160 symbols" do
      message = FactoryGirl.create(:message, text: "1"*160)
      expect(message.sms_count).to eq(1)
    end

    it "should contain 2 messages with 161 symbols" do
      message = FactoryGirl.create(:message, text: "1"*161)
      expect(message.sms_count).to eq(2)
    end

    it "should contain 3 messages with 307 symbols because there are 153 symbols in multipart message" do
      message = FactoryGirl.create(:message, text: "1"*307)
      expect(message.sms_count).to eq(3)
    end

    it "should contain 2 messages with 135 symbols because there are 67 symbols in multipart cyrillic message" do
      message = FactoryGirl.create(:message, text: "1"*134 + "А")
      expect(message.sms_count).to eq(3)
    end

    it "should set status to message" do
      message = FactoryGirl.create(:message, text: "1"*134 + "А")
      expect do
        message.set_status(:error)
      end.to change{message.status}.to('error')
    end
  end

  # describe "start messaging method" do
  #   let(:message){ FactoryGirl.create(:message, user: user) }

  #   before(:each) do
  #     allow(message).to receive(:contacts).and_return([double()])
  #   end

  #   it "should charge user by sms count" do
  #     expect do
  #       message.start
  #       user.reload
  #     end.to change{user.sms_amount}.by(-1)
  #   end

  #   it "should set status to suspended if user has not enough sms" do
  #     allow(message).to receive(:contacts).and_return([double(), double()])
  #     expect do
  #       message.start
  #     end.to change{message.status}.to('suspended')
  #   end

  #   it "should set status to queue if user has enough sms" do
  #     message.update_attributes(status: :suspended)
  #     expect do
  #       message.start
  #       message.reload
  #     end.to change{message.status}.to('queue')
  #   end

  #   it "should raise not persisted error if message is not new" do
  #     expect do
  #       Message.new.start
  #     end.to raise_error(Message::NotPersisted)
  #   end
  # end
end
