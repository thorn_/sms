# -*- encoding: utf-8 -*-
require 'rails_helper'

describe ContactMessage do
  let(:user){FactoryGirl.create(:user)}
  let(:attr){ {contact_id: 1, message_id: 1} }

  it "should create contact message with valid attributes" do
    expect do
      ContactMessage.create!(attr)
    end.to change{ContactMessage.count}.by(1)
  end

  describe "relations" do
    it "should belong to message" do
      expect(ContactMessage.new(attr)).to respond_to(:message)
    end

    it "should belong to contact" do
      expect(ContactMessage.new(attr)).to respond_to(:contact)
    end
  end

  describe "methods" do

    it "should set status of contact message by it's job id" do
      contact_message = ContactMessage.create(attr.merge(jid: "job id"))
      ContactMessage.status('job id', 'error', 'status message')
      contact_message.reload
      expect(contact_message.status).to eq('error')
      expect(contact_message.status_message).to eq('status message')
    end

  end
end
