require 'rails_helper'

describe Group do
  let(:user){FactoryGirl.create(:user)}
  let(:attr){{name: 'My Group', user_id: user.id}}
  before(:each) {user.main_group.destroy}

  it "should create group with right attributes" do
    expect{Group.create(attr)}.to change{Group.count}.by(1)
  end

  context 'associations' do
    it "should have many contacts" do
      expect(Group.new).to respond_to(:contacts)
    end
  end

  context 'validations' do
    it "should be invalid with nil name" do
      expect(Group.new(name: nil, user_id: user.id)).not_to be_valid
    end
  end

  context 'method' do
    let(:contact1) { FactoryGirl.create(:contact) }
    let(:contact2) { FactoryGirl.create(:contact) }
    context "clear" do
      before(:each) do
        @group = FactoryGirl.create(:group, editable: false)
        @group.contacts.push [contact1, contact2]
        @group.save
      end

      it "should make contact hidden if group is not editable" do
        contact1
        expect do
          @group.clear
          @group.reload
          contact1.reload
        end.to change{contact1.hidden}.from(false).to(true)
      end

      it "should not delete all contacts if group is editable" do
        @group = FactoryGirl.create(:group, editable: true)
        expect do
          @group.clear
        end.not_to change{Contact.count}
      end
    end
  end
end
