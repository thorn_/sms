require 'rails_helper'

describe Contact do
  let(:user){FactoryGirl.create(:user)}
  let(:attr){{number: FactoryGirl.generate(:number), user_id: user.id, hidden: false}}

  it "should create a record with valid attributes" do
    expect do
      Contact.create!(attr)
    end.to change{Contact.count}.by(1)
  end

  describe "validations" do
    it "should deny record without number" do
      expect(Contact.new(attr.merge(number: nil))).not_to be_valid
    end

    it "should deny numbers with invalid pattern" do
      %w{ 988123 8888888888 13456789012 +323455676 sandwich}.each do |invalid_number|
        expect(Contact.new(attr.merge(number: invalid_number))).not_to be_valid
      end
    end

    it "should not create a record without user" do
      expect(Contact.new(attr.merge(user_id: nil))).not_to be_valid
    end
  end

  describe "relations" do
    it "should belong to user" do
      expect(Contact.new).to respond_to(:user)
    end

    it "should have many contact messages" do
      expect(Contact.new).to respond_to(:contact_messages)
    end

    it "should have many messages" do
      expect(Contact.new).to respond_to(:messages)
    end

    it "should have many contact groups" do
      expect(Contact.new).to respond_to(:contact_groups)
    end

    it "should have many groups" do
      expect(Contact.new).to respond_to(:groups)
    end
  end

  describe "methods" do
    it "should return opened records" do
      contact = Contact.create(attr)
      hidden_contact = Contact.create(attr.merge(hidden: true))
      expect(Contact.opened).to include(contact)
      expect(Contact.opened).not_to include(hidden_contact)
    end
  end
end
