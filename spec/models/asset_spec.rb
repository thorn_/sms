require 'rails_helper'
include ActionDispatch::TestProcess

describe Asset do
  let(:file_name){ "/files/example.xls" }
  let(:file){ fixture_file_upload(file_name, "application/octet-stream") }
  let(:user){ FactoryGirl.create(:user) }
  let(:attr) {
    {
      file: file
    }
  }
  it "should create record with valid attributes" do
    expect{Asset.create!(attr)}.to change{Asset.count}.by(1)
  end

  describe "relations" do
    it "should belong to user" do
      expect(Asset.new).to respond_to(:user_id)
    end
  end

  describe "validations" do
    it "should reject record if there is no file" do
      expect(Asset.new).not_to be_valid
    end
  end

end
