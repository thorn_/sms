require 'rails_helper'

describe ContactGroup do
  let(:group){FactoryGirl.create(:group)}
  let(:contact){FactoryGirl.create(:contact)}
  let(:attr){{group_id: group.id, contact_id: contact.id}}

  it "should create record with valid attributes" do
    expect{ContactGroup.create(attr)}.to change{ContactGroup.count}.by(1)
  end
end
