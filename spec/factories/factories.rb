# -*- encoding: utf-8 -*-
FactoryGirl.define do

  factory :user do
    email
    password 'password'
    password_confirmation 'password'
    access_token 'access_token'
    role 'user'
    sms_amount 10
  end

  factory :contact do
    number
    user
    hidden false
    surname 'surname'
    last_name 'last_name'
  end

  factory :group do
    name "group name"
    user
    editable true
  end

  factory :message do
    text "message text"
    url "message url"
    user
    status "queue"
    status_message "in progress"
    send_date Date.today
    name "Message name"
    sms_to_send 1
  end

  factory :contact_message do
    contact
    message
    status "queue"
    status_message "status message"
    service_id "sid"
  end

  sequence :number do |n|
    "9%09d" % n
  end

  sequence :url do |n|
    "url#{n}.example.com"
  end

  sequence :email do |n|
    "example_#{n}@example.com"
  end

  factory :asset do
    file { fixture_file_upload("spec/fixtures/files/example.xls", "application/vnd.ms-excel") }
    user
  end

end
