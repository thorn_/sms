#!/usr/bin/env ruby
require 'rubygems'
require 'amqp'
# You might want to change this
ENV["RAILS_ENV"] ||= "development"

root = File.expand_path(File.dirname(__FILE__))
root = File.dirname(root) until File.exists?(File.join(root, 'config'))
Dir.chdir(root)

require File.join(root, "config", "environment")

$running = true
Signal.trap("TERM") do
  $running = false
end

while($running) do
  EventMachine.run do
    channel = AMQP::Channel.new(AMQP.connect())
    # exchange = channel.topic("message_worker", auto_delete: true)
    # channel.queue("message_worker").bind(exchange).subscribe do |headers, payload|
    #   message = Message.find(payload.to_i)
    #   return if message.suspended?
    #   message.set_status(:queue, "В обработке")
    #   message.contact_messages.each do |contact_message|
    #     contact = contact_message.contact
    #     Rabbitq::Client.new.publish('sender_channel', {id: contact_message.id, text: message.text, number: "7#{contact.number}", sender: "79083689305"}.to_json, {routing_key: 'default'})
    #   end
    # end

    # import_worker = channel.topic("import_worker", auto_delete: true)
    # channel.queue('import_worker').bind(import_worker).subscribe do |headers, payload|
    #   params = JSON.parse(payload)
    #   ContactImporter.import(params)
    # end

    # message_creator_worker = channel.topic("message_creator_worker", auto_delete: true)
    # channel.queue('message_creator_worker').bind(message_creator_worker).subscribe do |headers, payload|
    #   params = ActionController::Parameters.new(JSON.parse(payload))
    #   message = Message.find(params['id'])
    #   user = User.find(params['user_id'])
    #   all_contacts = ContactExtractor.extract(params, user)
    #   puts all_contacts.count
    #   message.contacts = all_contacts
    #   message.save
    #   message.start
    # end
  end
end
