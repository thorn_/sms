#!/usr/bin/env ruby
require 'smpp'
require 'amqp'
require 'json'

LOGFILE = File.dirname(__FILE__) + "/../../log/sms_gateway.log"
Smpp::Base.logger = Logger.new(LOGFILE)


# You might want to change this
ENV["RAILS_ENV"] ||= "development"

root = File.expand_path(File.dirname(__FILE__))
root = File.dirname(root) until File.exists?(File.join(root, 'config'))
Dir.chdir(root)
require File.join(root, "config", "environment")

$running = true
Signal.trap("TERM") do
  $running = false
end

class TokenBucket
  attr_reader :rate
  def initialize(rate)
    @rate = rate
    refresh
  end

  def refresh
    @tokens = rate
  end

  def take(number)
    if @tokens - number >= 0
      @tokens -= number
    else
      sleep(1)
      return false
    end
  end
end

while($running) do
  begin
    puts "Starting SMS Gateway. Please check the log at #{LOGFILE}"
    smarts = Rails.application.secrets.smarts
    config = {
      :host => smarts['host'],
      :port => smarts['port'],
      :system_id => smarts['system_id'],
      :password => smarts['password'],
      :system_type => '', # default given according to SMPP 3.4 Spec
      :interface_version => 52,
      :source_ton  => 0,
      :source_npi => 3,
      :destination_ton => 1,
      :destination_npi => 1,
      :source_address_range => '',
      :destination_address_range => '',
      :enquire_link_delay_secs => 10
    }
    EventMachine::run do
      tx = EventMachine::connect(
        config[:host],
        config[:port],
        Smpp::Transceiver,
        config,
        MessageHandler.new
      )

      # publish exchange
      connection = AMQP.connect
      channel = AMQP::Channel.new(connection)
      channel.prefetch(10)

      token_bucket = TokenBucket.new(40)
      EM.add_periodic_timer(1) { token_bucket.refresh }

      exchange = channel.topic("messages", auto_delete: true)

      # Start consuming MT messages (in this case, from the console)
      # Normally, you'd hook this up to a message queue such as Starling
      # or ActiveMQ via STOMP.
      publish_exchange = channel.topic("sender", auto_delete: true)

      channel.queue("sender").bind(publish_exchange).subscribe(ack: true) do |headers, payload|
        EM.defer(
          lambda {
            token_bucket.take(1)
            puts "We should send sms: #{payload}"
            params = JSON.parse(payload)
            receiver = params["number"]
            id = params["id"]
            sender = Rails.application.secrets.smarts['sender']
            if params["text"].ascii_only?
              body = params["text"]
              tx.send_mt id, sender, receiver, body
            else
              body = params["text"].encode("UTF-16BE").force_encoding("BINARY")
              tx.send_mt id, sender, receiver, body, data_coding: 8
            end
            headers.ack
          }
        )
      end
    end
    puts "No connection, reconnecting in 5 seconds"
    sleep 5
  rescue Exception => ex
    puts "Exception in SMS Gateway: #{ex} at #{ex.backtrace.join("\n")}"
  end
end
