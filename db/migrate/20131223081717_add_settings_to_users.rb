class AddSettingsToUsers < ActiveRecord::Migration
  def up
    add_column :users, :settings, :hstore
    execute("CREATE INDEX users_settings ON users USING GIN(settings)")
  end

  def down
    execute("DROP INDEX users_setting")
    remove_column :users, :settings
  end
end
