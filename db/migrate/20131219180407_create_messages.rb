class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text    :text, null: false
      t.string  :url
      t.integer :user_id
      t.integer :status, default: 0
      t.integer :sms_to_send, default: 0
      t.text    :status_message
      t.string  :name
      t.datetime :send_date

      t.timestamps
    end
  end
end
