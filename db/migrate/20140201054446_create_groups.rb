class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :user_id, null: false
      t.boolean :editable, default: true

      t.timestamps
    end
  end
end
