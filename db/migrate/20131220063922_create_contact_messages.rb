class CreateContactMessages < ActiveRecord::Migration
  def change
    create_table :contact_messages do |t|
      t.integer :contact_id, null: false
      t.integer :message_id, null: false
      t.string  :jid
      t.string  :service_id
      t.integer :status, default: 0
      t.text    :status_message

      t.timestamps
    end
  end
end
