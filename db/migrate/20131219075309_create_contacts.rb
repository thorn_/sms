class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :number, null: false
      t.string :name
      t.integer :user_id
      t.boolean :hidden, nil: false, default: false
      t.string :surname
      t.string :last_name
      t.timestamps
    end
  end
end
