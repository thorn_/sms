class AddFieldsToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.column :access_token, :string, unique: true
      t.column :role, :string, null: false, default: 'user'
      t.column :sms_amount, :integer, null: false, default: 0
    end
  end
end
