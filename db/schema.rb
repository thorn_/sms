# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140410172726) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "assets", force: true do |t|
    t.integer  "user_id"
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contact_groups", force: true do |t|
    t.integer "contact_id", null: false
    t.integer "group_id",   null: false
  end

  add_index "contact_groups", ["contact_id"], name: "index_contact_groups_on_contact_id", using: :btree
  add_index "contact_groups", ["group_id"], name: "index_contact_groups_on_group_id", using: :btree

  create_table "contact_messages", force: true do |t|
    t.integer  "contact_id",                 null: false
    t.integer  "message_id",                 null: false
    t.string   "jid"
    t.string   "service_id"
    t.integer  "status",         default: 0
    t.text     "status_message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts", force: true do |t|
    t.string   "number",                     null: false
    t.string   "name"
    t.integer  "user_id"
    t.boolean  "hidden",     default: false
    t.string   "surname"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "name"
    t.integer  "user_id",                   null: false
    t.boolean  "editable",   default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.text     "text",                       null: false
    t.string   "url"
    t.integer  "user_id"
    t.integer  "status",         default: 0
    t.integer  "sms_to_send",    default: 0
    t.text     "status_message"
    t.string   "name"
    t.datetime "send_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "access_token"
    t.string   "role",                   default: "user", null: false
    t.integer  "sms_amount",             default: 0,      null: false
    t.hstore   "settings"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["settings"], name: "users_settings", using: :gin

end
