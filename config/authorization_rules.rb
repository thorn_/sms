authorization do
  role :registrant do
    has_permission_on :devise_registrations, to: :manage
    has_permission_on :registrations, to: :manage
    has_permission_on :devise_sessions, to: :manage
    has_permission_on :devise_passwords, to: :manage
  end

  role :guest do
    includes :registrant
    has_permission_on :pages, to: :read
    has_permission_on :templates, to: :read
    has_permission_on :application, to: :read
    has_permission_on :api_v1_messages, to: [:set_status]
  end

  role :user do
    includes :guest
    has_permission_on :api_v1_messages, to: [:manage, :send_message]
    has_permission_on :api_v1_contacts, to: [:manage]
    has_permission_on :api_v1_groups, to: [:manage]
  end

  role :moderator do
    includes :user
  end

  role :admin do
    includes :moderator
    has_permission_on :admin_users, to: :manage
    has_permission_on :switch_user, to: :set_current_user
  end

end

privileges do
  privilege :manage, includes: [:create, :read, :update, :delete]
  privilege :read,   includes: [:index, :show]
  privilege :create, includes: [:new, :add]
  privilege :update, includes: :edit
  privilege :delete, includes: [:destroy, :del, :clear]
end
