# require 'amqp'
# RMQ_CONFIG = YAML.load_file("#{Rails.root}/config/secrets.yml")[Rails.env]['amqp']

# module AdmitEventMachine
#   def self.start
#     if defined?(PhusionPassenger)
#       PhusionPassenger.on_event(:starting_worker_process) do |forked|
#         if forked && EM.reactor_running?
#           EM.stop
#         end
#         Thread.new {
#           EM.run do
#             config_connect_open_channel
#           end
#         }
#         die_gracefully_on_signal
#       end
#     else #Development
#       Thread.abort_on_exception = true
#         Thread.new {
#           EM.run do
#             if EM.reactor_running?
#               config_connect_open_channel
#             end
#           end
#         } unless defined?(Thin)
#     end
#   end

#   def self.config_connect_open_channel
#     AMQP.connection ||= AMQP.connect(self.conf)
#     AMQP.channel ||= AMQP::Channel.new(AMQP.connection)
#   end

#   def self.conf
#     connection_settings = {:host     => RMQ_CONFIG['server'],
#                            :port     => RMQ_CONFIG['port'],
#                            :vhost    => RMQ_CONFIG['vhost'],
#                            :user     => RMQ_CONFIG['user'],
#                            :password => RMQ_CONFIG['pass'],
#                            :timeout  => 0.3}
#   end

#   def self.die_gracefully_on_signal
#     Signal.trap("INT")  { EM.stop }
#     Signal.trap("TERM") { EM.stop }
#   end
# end

# AdmitEventMachine.start
