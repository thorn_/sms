namespace :environment_setup do
  desc "Setup environment variables for oauth"
  task :set, roles: :app do
    template 'private/secrets.yml.erb', "#{current_path}/config/secrets.yml"
  end
  after "deploy:cold", "environment_setup:set"
end
