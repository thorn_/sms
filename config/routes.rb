Sms::Application.routes.draw do
  devise_for :users, :controllers => {:registrations => "registrations"}

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :messages do
        post :set_status, on: :collection
        get :send_message, on: :collection
        get :restart, on: :member
      end
      resources :assets
      resources :contacts do
        get :add, on: :collection
        get :del, on: :collection
      end
      resources :groups do
        post :clear, on: :member
      end
    end
  end

  namespace :admin do
    resources :users
    root to: "users#index"
  end
  get '/pages/:id' => 'pages#show'
  get '/templates/:controller_name/:id' => "templates#show"

  get 'switch_user' => 'switch_user#set_current_user'

  root to: "application#index"
end
