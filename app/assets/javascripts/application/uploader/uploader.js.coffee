sms.factory 'Asset', ($resource, currentState) ->
  $resource '/api/v1/assets/:id.json', {access_token: currentState.user.access_token, id: '@id'}

sms.controller 'smsUploaderCtrl', ($scope, $rootScope, Asset, $fileUploader, currentState) ->
  $scope.currentState = currentState
  # $scope.assets = Asset.query ->
  #   currentState.user.assets = $scope.assets

  $scope.delete = (asset) ->
    asset.$delete ->
      $scope.assets.splice($scope.assets.indexOf(asset), 1);

  $scope.uploader = $fileUploader.create
    autoUpload: true
    scope: $scope
    url: "/api/v1/assets.json?access_token=#{$scope.currentState.user.access_token}"
    alias: 'asset[file]'

  $scope.uploader.bind 'beforeupload', (event, item) ->
    NProgress.start()
    group = $scope.currentState.group
    item.url += "&group_id=#{group.id}" if group

  $scope.uploader.bind 'success', (event, xhr, item, response) ->
    $rootScope.$emit 'alert', 'Файл загружен. Номера появятся в течение трех минут.', 'success'
    # $scope.assets.push(new Asset(response))
    NProgress.done()
    $scope.uploader.removeFromQueue(item)

  $scope.uploader.bind 'progress', (event, item, progress) ->
    NProgress.set(progress/100)

  $scope.uploader.bind 'error', (event, xhr, item, response) ->
    NProgress.done()
    $rootScope.emit 'alert', 'Ошибка загрузки файла', 'error'
    $scope.uploader.removeFromQueue(item)
