window.get_current_user = ->
  if window.gon && window.gon.current_user
    gon.current_user.signed_in = true
    return gon.current_user
  else
    return {id: -1, email: "guest@example.com", access_token: "access_token", signed_in: false}

sms.value 'currentState',
  user: window.get_current_user()
  groups: []
  contacts: []
  page: 1
  alerts: []

# http://stackoverflow.com/questions/14833326/how-to-set-focus-in-angularjs
sms.directive 'focusMe', ['$timeout', '$parse', ($timeout, $parse) ->
  # scope: true   # optionally create a child scope
  link: (scope, element, attrs) ->
    model = $parse(attrs.focusMe)
    scope.$watch model, (value) ->
      if value is true
        $timeout ->
          element[0].focus()
        , 200
    element.bind 'blur', ->
       scope.$apply(model.assign(scope, false))
]
