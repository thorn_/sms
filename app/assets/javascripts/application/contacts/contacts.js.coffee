sms.factory 'Contact', ['$resource', 'currentState',  ($resource, currentState) ->
  $resource('/api/v1/contacts/:id',
    {access_token: currentState.user.access_token, id: '@id'},
    {update: {method:'PATCH'}})
]

sms.controller 'smsContactsCtrl', ['$scope', '$rootScope', '$window', '$modal', 'filterFilter', 'Contact', 'Group', 'currentState', '$http', ($scope, $rootScope, $window, $modal, filterFilter, Contact, Group, currentState, $http) ->
  $scope.currentState = currentState
  $scope.searchQuery = ''
  $scope.selected = {contacts: [], group: {}, allContacts: false}

  $scope.getContacts = (params = {}) ->
    NProgress.start()
    angular.extend(params, {page: currentState.page})
    angular.extend(params, {"q[groups_id_in]": $scope.currentState.group.id}) if currentState.group?.id
    Contact.query params, (contacts) ->
      NProgress.done()
      $scope.contacts = contacts
      $scope.selected.allContacts = false
      $scope.contactsPagination = contacts?[0]?.pagination
      $scope.currentState.contacts = $scope.contacts
      $scope.$watch('contacts|filter:{selected:true}', (nv) ->
        $scope.selected.contacts = nv
      , true)
    , (data) ->
      NProgress.done()

  $scope.editContact = (contact) ->
    if contact is 'new'
      $scope.contact = new Contact()
    else
      $scope.contactBackup = angular.copy(contact)
      $scope.contact = contact
    $scope.showForm()

  $scope.saveContact = (contact) ->
    if contact.id
      contact.$update (data) ->
        $rootScope.$emit 'contacts.edited', data
      , (data) ->
        $rootScope.$emit 'alert', 'Неверные данные', 'error'
        contact.$get()
    else
      contact.$save (data) ->
        $scope.contacts.unshift(data)
        $rootScope.$emit 'contacts.saved', data
      , (data) ->
        $rootScope.$emit 'alert', 'Не удалось сохранить контакт', 'error'

  $scope.deleteContact = (contact) ->
    $scope.deletedContactBackup = angular.copy contact
    contact.$delete {from_group: $scope.currentState.group.id}, (data) ->
      data.group_ids = $scope.deletedContactBackup.group_ids
      $rootScope.$emit 'contacts.deleted', data
    , (data) ->
      $scope.getContacts()
      $rootScope.$emit 'alert', data.errors, 'error'
    index=$scope.contacts.indexOf(contact)
    $scope.contacts.splice(index,1)

  $scope.showForm = ->
    modalInstance = $modal.open
      templateUrl: '/templates/contacts/_contact_form.html'
      scope: $scope
      controller: ($scope, $modalInstance) ->
        $scope.shouldBeOpen = true

        $scope.ok = ->
          $modalInstance.close($scope.contact)

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')

    modalInstance.result.then (contact) ->
      contact.group_ids = $scope.currentState.group.id
      $scope.saveContact(contact)
    , ->
      angular.copy($scope.contactBackup, $scope.contact);
      console.log 'Modal dismissed at: ' + new Date()

  $scope.copyToGroupForm = ->
    modalInstance = $modal.open
      templateUrl: '/templates/contacts/_contact_group_form.html'
      scope: $scope
      controller: ($scope, $modalInstance) ->
        $scope.selected.group = $scope.currentState.groups[0]
        $scope.shouldBeOpen = true

        $scope.ok = ->
          $modalInstance.close($scope.selected.group)

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')

    modalInstance.result.then (group) ->
      $scope.copyToGroup(group)
    , ->
      console.log 'Modal dismissed at: ' + new Date()

  $scope.confirmGroupClear = ->
    modalInstance = $modal.open
      templateUrl: '/templates/groups/_clear_group_confirmation.html'
      scope: $scope
      controller: ($scope, $modalInstance) ->
        $scope.ok = ->
          $modalInstance.close($scope.currentState.group)
        $scope.cancel = ->
          $modalInstance.dismiss('cancel')
    modalInstance.result.then (group) ->
      group.$clear (data) ->
        $scope.getContacts()
        $rootScope.$emit 'contacts.deletedSelected'
      , (data) ->
        $rootScope.emit 'alert', data.errors, 'error'
    , ->
      console.log 'Modal dismissed at: ' + new Date()

  $scope.copyToGroup = (group) ->
    for contact in $scope.selected.contacts
      cnt = new Contact(contact)
      cnt.group_ids = group.id
      $scope.saveContact(cnt)

  $scope.deleteSelected = ->
    $scope.deleteContact(contact) for contact in $scope.selected.contacts
    $rootScope.$emit 'contacts.deletedSelected'

  $scope.setPage = (number) ->
    $scope.currentState.page = number
    $scope.getContacts()

  $scope.contact_num = (n) ->
    return n + " контакт" +
      (
        if (n%10 < 5 && n%10 > 0 && (n%100 < 5 || n%100 > 20))
          if n%10 > 1
            "a"
          else
            ""
        else
          "ов"
      )

  $scope.download = ->
    NProgress.start()
    group = $scope.currentState.group
    url = "/api/v1/contacts.xls?access_token=#{$scope.currentState.user.access_token}"
    url += "&q[groups_id_in]=#{group.id}" if group
    date = new Date()
    date_string = "#{date.getDate()}_#{date.getMonth() + 1}_#{date.getFullYear()}"
    $scope.url = url
    $window.open(url)
    NProgress.done()

  $scope.refresh = ->
    $scope.getContacts()
    $rootScope.$emit 'contacts.refreshed'

  $scope.search = (query) ->
    params = {'q[number_or_name_or_surname_or_last_name_start]': query}
    $scope.getContacts(params)

  $scope.$watch 'selected.allContacts', (nv) ->
    if $scope.contacts
      contact.selected = nv for contact in $scope.contacts

  $scope.$watch 'currentState.group', (group) ->
    $scope.getContacts() if group
]
