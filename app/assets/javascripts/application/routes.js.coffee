window.routes =
  #-------- Documentation --------#
  'documentation':
    url: "/documentation"
    abstract: true
    templateUrl: '/templates/documentation/index.html'
    data:
      feature: 'documentation'
  'documentation.contents':
    url: '/contents'
    templateUrl: '/templates/documentation/contents.html'
    data:
      feature: 'documentation'
  'documentation.send_message':
    url: "/send_message"
    templateUrl: '/templates/documentation/send_message.html'
    data:
      feature: 'documentation'
  'documentation.contacts':
    url: "/contacts"
    templateUrl: '/templates/documentation/contacts.html'
    data:
      feature: 'documentation'
  'documentation.add_contact':
    url: "/add_contact"
    templateUrl: '/templates/documentation/add_contact.html'
    data:
      feature: 'documentation'
  'documentation.delete_contact':
    url: "/delete_contact"
    templateUrl: '/templates/documentation/delete_contact.html'
    data:
      feature: 'documentation'

  #-------- Messages --------
  "messages":
    url:         '/messages'
    templateUrl: '/templates/messages/index.html'
    controller:  'smsMessagesCtrl'
    data:
      feature: 'messages'
  "messages_show":
    url:         '/messages/:messageId'
    templateUrl: '/templates/messages/show.html'
    controller:  'smsMessageCtrl'
    data:
      feature: 'messages'

  #-------- Contacts --------
  "contacts":
    url:         '/contacts'
    templateUrl: '/templates/contacts/index.html'
    abstract: true
    data:
      feature: 'contacts'

  "contacts.list":
    url:         '/'
    views:
      'contacts_table':
        templateUrl: '/templates/contacts/_contacts_table.html'
        controller: 'smsContactsCtrl'
      'groups_aside':
        templateUrl: '/templates/contacts/_groups_aside'
        controller: 'smsGroupsCtrl'
    data:
      feature: 'contacts'

window.sms.config ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise("/contacts/")
  for route, params of window.routes
    $stateProvider.state route, params
