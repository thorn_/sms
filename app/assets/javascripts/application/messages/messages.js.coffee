sms.factory 'Message', ['$resource', 'currentState', ($resource, currentState) ->
  $resource '/api/v1/messages/:id', {access_token: currentState.user.access_token, id: '@id'}, {'restart': {method: 'GET', isArray: false, url: '/api/v1/messages/:id/restart'}}
]

sms.controller 'smsMessagesCtrl', ['$scope', '$rootScope', '$state', '$modal', 'currentState', 'Group', 'Contact', 'Message', ($scope, $rootScope, $state, $modal, currentState, Group, Contact, Message) ->
  $scope.$state = $state
  $scope.currentState = currentState
  $scope.searchQuery = ''
  $scope.defaultSelected = {group_ids: [], contact_ids: [], groups: [], group_ids: [], contacts: [], numbers: [], messages: [], group_contacts_number: 0}
  $scope.selected = angular.copy $scope.defaultSelected
  $scope.message = {numbers: "", text: "",messages_count: 0, symbols_till_limit: 0}

  $scope.$watch 'searchQuery', (nv) ->
    console.log nv

  $scope.getMessages = (parameters = {page: currentState.page}) ->
    NProgress.start()
    parameters['q[text_or_name_cont]'] = $scope.searchQuery if $scope.searchQuery.length > 0
    Message.query parameters, (messages) ->
      NProgress.done()
      $scope.messages = messages
      $scope.currentState.messages = $scope.messages
      $scope.messagesPagination = messages?[0]?.pagination
      $scope.$watch('messages|filter:{selected:true}', (nv) ->
        $scope.selected.messages = nv
      , true)
    , (data) ->
      NProgress.done()

  $scope.getGroups = ->
    Group.query (groups) ->
      $scope.groups = groups
      $scope.currentState.groups = $scope.groups
      $scope.$watch('groups|filter:{selected:true}', (nv) ->
        $scope.selected.groups = nv
        contact_counts = (group.contacts_count for group in nv)
        gcn = contact_counts.reduce (previousValue, currentValue, index, array) ->
          return previousValue + currentValue
        , 0
        $scope.selected.group_contacts_number =  gcn
      , true)

  $scope.getMessages()

  $scope.saveMessage = (message) ->
    NProgress.start()
    mess = new Message(message)
    mess.numbers = $scope.selected.numbers
    mess.group_ids = $scope.selected.group_ids
    mess.contact_ids = (contact.id for contact in $scope.selected.contacts)
    mess.$save (data) ->
      NProgress.done()
      $scope.messages.unshift data
    , (data) ->
      NProgress.done()

  $scope.deleteMessage = (message) ->
    $scope.deletedMessageBackup = angular.copy message
    message.$delete (data) ->
      $rootScope.$emit 'messages.deleted', data
    , (data) ->
      $scope.getMessages()
      $rootScope.$emit 'alert', data.errors, 'error'
    index=$scope.messages.indexOf(message)
    $scope.messages.splice(index,1)

  $scope.setPage = (number) ->
    $scope.currentState.page = number
    $scope.getMessages()

  $scope.newMessage = ->
    $scope.getGroups()
    modalInstance = $modal.open
      templateUrl: '/templates/messages/_message_form.html'
      scope: $scope
      controller: ($scope, $modalInstance) ->
        $scope.shouldBeOpen = true

        $scope.ok = ->
          $modalInstance.close($scope.message)

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')

    modalInstance.result.then (message) ->
      $scope.saveMessage(message)
      angular.copy $scope.selected, $scope.defaultSelected
    , ->
      console.log 'Modal dismissed at: ' + new Date()
      angular.copy $scope.selected, $scope.defaultSelected

  $scope.deleteSelected = ->
    $scope.deleteMessage(message) for message in $scope.selected.messages
    $scope.selected.allMessages = false
    $rootScope.$emit 'messages.deletedSelected'
    $scope.getMessages()

  $scope.deleteAllMessages = ->
    $scope.deleteMessage(message) for message in $scope.messages
    $scope.selected.allMessages = false
    $rootScope.$emit 'messages.deletedSelected'
    $scope.getMessages()

  $scope.search = (query) ->
    $scope.getMessages({'q[text_or_name_cont]': query})

  $scope.groupsSelect =
    width: '400px'
    dropdownAutoWidth: false
  $scope.contactsSelect =
    width: '400px'
    dropdownAutoWidth: false
    ajax:
      url: "/api/v1/contacts"
      data: (term, page) ->
        return {access_token: currentState.user.access_token, page: page, 'q[number_or_name_or_surname_or_last_name_cont]': term}
      results: (data, page) ->
        page = data?[0]?.pagination?.currentPage || 1
        total = data?[0]?.pagination?.totalPages || 1
        more = page < total
        return {results: data, more: more}
    formatResult: (contact) ->
      contact.number + ' ' + (contact.full_name or '')
    formatSelection: (contact) ->
      contact.number + ' ' + (contact.full_name or '')

  $scope.restartMessage = (message) ->
    message.$restart (data) ->
      $rootScope.$emit 'alert', data.status_message
    , (data) ->
      $rootScope.$emit 'alert', data.errors, 'error'

  $scope.$watch 'selected.allMessages', (nv) ->
    if $scope.messages
      contact.selected = nv for contact in $scope.messages

  $scope.$watch 'message.numbers', (nv) ->
    numbers = nv.split("\n")
    $scope.selected.numbers = []
    for number in numbers
      number = number.replace /^\s+|\s+$/g, ""
      if number.match(/\d{10}$/)
        $scope.selected.numbers.push number

  $scope.$watch 'message.text', (nv) ->
    text = nv?.replace /^\s+|\s+$/gm, ""
    if !text or text.length is 0
      symbols_in_message = 0
      mc = 0
    else
      cyr = true if text.match(/[А-я]+/m)
      symbols_in_message = if cyr then 70 else 160
      if text.length > 70 and cyr
        symbols_in_message = 67
      else if text.length > 160 and !cyr
        symbols_in_message = 153

      mc = Math.ceil(text.length / symbols_in_message )
    $scope.message.messages_count = mc
    $scope.message.symbols_in_message = symbols_in_message

  $scope.$watch 'selected.group_ids', (nv) ->
    for group in ($scope.groups || [])
      group.selected = nv.indexOf(group.id.toString()) isnt -1

  $scope.$watch 'selected.contact_ids', (nv) ->
    $scope.selected.contacts = nv

]

sms.controller 'smsMessageCtrl', ['$scope', '$rootScope', '$state', 'currentState', 'Message', ($scope, $rootScope, $state, currentState, Message) ->
  $scope.currentState = currentState
  $scope.searchQuery = ''
  $scope.getMessage = (params = {})->
    NProgress.start()
    angular.extend params, {id: $state.params.messageId}
    angular.extend params, {page: currentState.page}
    Message.get params, (message) ->
      NProgress.done()
      $scope.message = message
      $scope.currentState.message = message
      $scope.contactMessages = message.contact_messages
      $scope.contactMessagesPagination = $scope.contactMessages?[0]?.pagination
    , (data) ->
      NProgress.done()
      $rootScope.$emit 'alert', data.data.errors, 'error'

  $scope.setPage = (number) ->
    $scope.currentState.page = number
    $scope.getMessage()

  $scope.getMessage()

  $scope.search = (query) ->
    $scope.getMessage({'q[contact_number_or_contact_name_or_contact_surname_or_contact_last_name_start]': query})

]
