#= require ./common/common
#= require ./messages/messages
#= require ./contacts/contacts
#= require ./groups/groups
#= require ./uploader/uploader
#= require ./routes

sms.controller 'smsApplicationCtrl', ['$scope', '$state', '$rootScope', 'currentState',  ($scope, $state, $rootScope, currentState) ->
  $scope.currentState = currentState
  $scope.user = currentState.user
  $scope.state = $state
  $scope.alerts = []

  $rootScope.$on 'alert', (event, message, type='info') ->
    message = message || "Произошла ошибка!"
    type = 'danger' if type is 'error'
    $scope.alerts.push msg: message, type: type

  $scope.closeAlert = (index) ->
    $scope.alerts.splice(index, 1)
]
