sms.factory 'Group', ['$resource', 'currentState', ($resource, currentState) ->
  $resource('/api/v1/groups/:id', {access_token: currentState.user.access_token, id: '@id'},
    {update: {method:'PATCH'}, clear: {url: '/api/v1/groups/:id/clear', method: "POST", isArray: false}})
]

sms.controller 'smsGroupsCtrl', ['$scope', '$rootScope', '$modal', 'currentState', 'Group', ($scope, $rootScope, $modal, currentState, Group) ->
  $scope.currentState = currentState

  $scope.getGroups = ->
    Group.query (groups) ->
      $scope.groups = groups
      $scope.mainGroup = group for group in $scope.groups when group.editable is false
      $scope.currentState.groups = $scope.groups
      $scope.currentState.mainGroup = $scope.mainGroup
      $scope.currentState.group = $scope.mainGroup unless $scope.currentState.group
      for group in groups
        if $scope.currentState.group.id is group.id
          $scope.currentState.group.contacts_count = group.contacts_count

  $scope.getGroups()

  $scope.editGroup = (group) ->
    $scope.showForm()
    if group is 'new'
      $scope.group = new Group(name: "New group")
    else
      $scope.groupBackup = angular.copy(group)
      $scope.group = group

  $scope.saveGroup = (group) ->
    if group.id
      group.$update (data) ->
        console.log "updated"
      , (data) ->
        $scope.getGroups()
        $rootScope.$emit 'alert', data.errors, 'error'
    else
      group.$save (data) ->
        $scope.groups.push(data)

  $scope.deleteGroup = (group) ->
    group.$delete (data) ->
      $scope.showContacts($scope.mainGroup) if $scope.currentState.group.id is data.id
      $rootScope.$emit 'groups.deleted', data
    , (data) ->
      $scope.getGroups()
      $rootScope.$emit 'alert', data.errors, 'error'
    index=$scope.groups.indexOf(group)
    $scope.groups.splice(index,1)

  $scope.confirmGroupDelete = (group) ->
    modalInstance = $modal.open
      templateUrl: '/templates/groups/_delete_group_confirmation.html'
      scope: $scope
      controller: ($scope, $modalInstance) ->
        $scope.ok = ->
          $modalInstance.close($scope.currentState.group)
        $scope.cancel = ->
          $modalInstance.dismiss('cancel')
    modalInstance.result.then (group) ->
      $scope.deleteGroup(group)
    , ->
      console.log 'Modal dismissed at: ' + new Date()

  $scope.showForm = ->
    modalInstance = $modal.open
      templateUrl: '/templates/groups/_group_form.html'
      scope: $scope
      controller: ($scope, $modalInstance) ->
        $scope.shouldBeOpen = true

        $scope.ok = ->
          $modalInstance.close($scope.group)

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')

    modalInstance.result.then (group) ->
      $scope.saveGroup(group)
    , ->
      angular.copy($scope.groupBackup, $scope.group);
      console.log 'Modal dismissed at: ' + new Date()

  $scope.showContacts = (group) ->
    $scope.currentState.group = group

  for ev in ['contacts.deleted', 'contacts.saved', 'contacts.edited']
    $rootScope.$on ev, (event, contact) ->
      group.$get() for group in $scope.groups when contact.group_ids.indexOf(group.id) isnt -1

  $rootScope.$on 'contacts.deletedSelected', (event, contact) ->
    $scope.getGroups()
  $rootScope.$on 'contacts.refreshed', (event, contact) ->
    $scope.getGroups()
]
