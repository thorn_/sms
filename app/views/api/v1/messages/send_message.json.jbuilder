json.status "ok"
json.message do
  json.partial! '/api/v1/messages/message', message: @message
end
