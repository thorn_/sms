json.contact_number contact_message.contact.number
json.contact_full_name "#{contact_message.contact.last_name} #{contact_message.contact.name} #{contact_message.contact.surname}"
json.created_at contact_message.created_at
json.updated_at contact_message.updated_at
json.status contact_message.status
if contact_message.pagination
  json.pagination contact_message.pagination
end
