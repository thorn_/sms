json.id @message.id
json.text @message.text
json.message_all @message.sms_to_send
json.message_success @message.contact_messages.success.count
json.message_queue @message.contact_messages.queue.count
json.message_error @message.contact_messages.error.count
json.created_at @message.created_at
json.sms_count @message.sms_count
json.sms_to_send @message.sms_to_send
json.status @message.status
json.status_message @message.status_message
json.created_at @message.created_at
json.updated_at @message.updated_at
json.contact_messages do
  json.partial! '/api/v1/messages/contact_message', collection: @contact_messages, as: :contact_message
end
