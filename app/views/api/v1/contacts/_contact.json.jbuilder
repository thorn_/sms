json.id contact.id
json.group_ids contact.group_ids
json.number contact.number
json.name contact.name
json.surname contact.surname
json.last_name contact.last_name
json.full_name "#{contact.last_name} #{contact.name} #{contact.surname}"
json.status 'ok'
if contact.pagination
  json.pagination contact.pagination
end
