require "rubygems"
require "amqp"

module Rabbitq
  class Client
    attr_accessor :thread

    def self.publish(exchange_name, json_string)
      new.publish exchange_name, json_string
    end

    def publish(exchange_name, json_string, options={})
      begin
        if EM.reactor_running?
          AMQP.channel ||= AMQP::Channel.new(AMQP.connection)
          EventMachine.next_tick do
            AMQP.channel.topic(exchange_name, auto_delete: true).publish(json_string, options)
          end
          return true
        else
         # Event Machine is not running!
         # requeue?
         return false
        end
      rescue Exception => exc
        puts exc
        # Store error / Email
        # Requeue sending e.g Delayed Job
        # Error Example: EventMachine::ConnectionError: unable to resolve server address
      end
    end

  end
end

# Call Method
# require 'rabbitq/client'
# Rabbitq::Client::publish "test", "testing.qq"

# New Instance of Class
# x = Rabbitq::Client.new
# x.publish "test", "testing.qq"

