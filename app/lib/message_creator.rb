# --- encoding: utf-8 ---
class MessageCreator
  def self.create(params, user)
    all_contacts = ContactExtractor.extract(params, user)
    return false unless all_contacts.any?
    message = user.messages.new(message_params(params))
    message.sms_to_send = all_contacts.count * message.sms_count
    message.set_status(:queue, Message::STATUS_MESSAGES[:creating])
    return false unless message.save
    Rabbitq::Client.new.publish 'messages', {
                                  id: message.id,
                                  contact_ids: params[:contact_ids],
                                  group_ids: params[:group_ids],
                                  numbers: params[:numbers],
                                  user_id: user.id
                                }.to_json,
                                {routing_key: "message.created"}
    message
  end

  def self.message_params(params)
    par = ActionController::Parameters.new(params)
    par.require(:message).permit(:text, :url, :name, :send_date)
  end
end
