class UserSmsPolicy
  attr_accessor :user, :contacts, :message
  def initialize(user, message)
    @user = user
    @contacts = message.contacts
    @message = message
  end

  def sms_to_send
    @sms_to_send ||= message.sms_count * contacts.count
  end

  def can_send_message?
    sms_to_send > 0 && user.sms_amount >= sms_to_send# && sms_to_send < 5
  end

end
