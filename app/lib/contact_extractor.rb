include ActionDispatch::TestProcess
require 'spreadsheet'

class ContactExtractor
  def self.extract(params, user)
    group_contacts = Contact.opened.joins(:groups).where(groups: {id: params[:group_ids], user_id: user.id})

    contacts = user.contacts.where(id: params[:contact_ids])

    numbers = self.parse_numbers(params[:numbers])
    new_contacts = numbers.map {|n| user.contacts.create(number: n, hidden: true)}

    [group_contacts + new_contacts + contacts].flatten.uniq {|item| item.number}
  end

  def self.parse_numbers(numbers = [])
    numbers = [] unless numbers
    numbers.flatten.uniq.compact
  end

  def self.parse_number(number)
    num = number.to_s.strip.chomp.gsub(/\.0\z/, '')
    result = num.match /^(?:(?:8|\+7)[\- ]?)?((\(?9\d{2}\)?[\- ]?)?[\d\- ]{9,14})$/
    if result && result[1] && result[1].length > 9
      num = result[1]
      num.scan(/\d/).join('')
      if (num[0] == '7' || num[0] == '8')
        num[1..10]
      else
        num
      end
    else
      false
    end
  end

  def self.extract_from_spreadsheet(sheet, user)
    book = Spreadsheet.open sheet
    contacts = []
    book.worksheets.each do |ws|
      ws.each do |row|
        contact = Contact.new(
          number: parse_number(row[0]),
          last_name: row[1],
          name: row[2],
          surname: row[3],
          user_id: user.id
        )
        contacts.push(contact) if contact.valid?
      end
    end
    contacts
  end
end
