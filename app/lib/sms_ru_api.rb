class SmsRuApi
  PHONE_REGEX = /\A9\d{9}\z/i
  API_REGEX = /\A\w{8}-\w{4}-\w{4}-\w{4}-\w{12}\z/m
  STATUSES = {error: 'error', success: 'success', queue: 'queue'}

  attr_reader :api_key, :testing

  def initialize(api_key=nil, testing=false)
    raise WrongApiKeyError unless api_key =~ API_REGEX
    @testing = 1 if testing
    @api_key = api_key
  end

  def send_message(text = nil, contact = nil)
    validate(text, contact)
    c = Curl.post("http://sms.ru/sms/send?api_id=#{api_key}", to: contact, text: text, test: testing)
    res = c.body.split("\n")
    SmsRuApi.get_status_from(res[0]).merge(service_id: res[1])
  rescue MessageError => e
    SmsRuApi.make_status_object(STATUSES[:error], e.message).merge(service_id: contact)
  end

  def status(service_id)
    c = Curl.get("http://sms.ru/sms/status?api_id=#{api_key}", id: service_id)
    code = c.body.blank? ? '104' : c.body
    SmsRuApi.get_status_from(code).merge(service_id: service_id)
  end

  def self.get_status_from(code)
    case code.to_s
    when '100'
      SmsRuApi.make_status_object(STATUSES[:queue], "1")
    when '101'
      SmsRuApi.make_status_object(STATUSES[:queue], "2")
    when '102'
      SmsRuApi.make_status_object(STATUSES[:queue], "3")
    when '103'
      SmsRuApi.make_status_object(STATUSES[:success], "4")
    else
      SmsRuApi.make_status_object(STATUSES[:error], code.to_s)
    end
  end

  def self.make_status_object(status, message)
    {status: status, service_message: message}
  end

  def validate(text, contact)
    raise(MessageError, "Empty text") if text.blank?
    raise(MessageError, "No contact given") if contact.nil? || contact.class != String || contact.length == 0
    raise(MessageError, "Wrong contact given") unless contact =~ PHONE_REGEX
  end

  class WrongApiKeyError < StandardError; end
  class MessageError < StandardError; end
end
