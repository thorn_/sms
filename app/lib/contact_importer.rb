class ContactImporter
  def self.import(params)
    user = User.find_by_id(params['user_id'])
    return false unless user
    file = File.open(params['file_path'])
    group = user.groups.find_by_id(params['group_id'])
    main_group = user.main_group if user.main_group
    groups = [group, main_group].compact.uniq
    contacts = ContactExtractor.extract_from_spreadsheet(file, user)
    contacts.each_slice(1000) {|batch| batch.each{ |c| c.groups.push(*groups)} }
    contacts.each_slice(1000) do |batch|
      ActiveRecord::Base.transaction do
        batch.each(&:save)
      end
    end
  end
end
