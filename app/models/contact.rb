# --- encoding: utf-8 ---
class Contact < ActiveRecord::Base
  validates :number, presence: true,format: { with: /\A9\d{9}\z/,
    message: "only allows letters" }
  validates :user_id, presence: true

  attr_accessor :pagination

  belongs_to :user
  has_many :contact_messages, dependent: :destroy
  has_many :messages, through: :contact_messages
  has_many :contact_groups, dependent: :destroy
  has_many :groups, through: :contact_groups, dependent: :destroy

  scope :opened, -> { where(hidden: false) }

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << ["Phone number", "First Name", "Last name", "Middle name"]
      all.each do |contact|
        csv << [contact.number, contact.name, contact.last_name, contact.surname]
      end
    end
  end
end
