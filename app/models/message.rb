# -*- encoding: utf-8 -*-
class Message < ActiveRecord::Base
  validates_presence_of :user_id
  validates :text, presence: true, length: {maximum: 1530, minimum: 1}
  STATUS_MESSAGES = {
                      creating: "Created",
                      queue: "In queue",
                      success: "Completed",
                      suspended: "Insufficient balance"
                    }
  enum status: [:queue, :success, :error, :suspended]

  belongs_to :user
  has_many :contact_messages, dependent: :destroy
  has_many :contacts, through: :contact_messages
  attr_accessor :test, :pagination

  def sms_count
    cyr = true if text.match(/[А-я]+/m)
    symbols_in_message = cyr ? 70 : 160
    if text.length > 70 and cyr
      symbols_in_message = 67
    elsif text.length > 160 and !cyr
      symbols_in_message = 153
    end
    (text.length/symbols_in_message.to_f).ceil
  end

  def set_status(status, status_message = '')
    self.update_attributes(status: status, status_message: status_message) if status
  end

  def start
    Rabbitq::Client.new.publish 'messages', self.id, {routing_key: 'message.start'}
    # raise NotPersisted unless self.persisted?
    # policy = UserSmsPolicy.new(self.user, self)
    # if policy.can_send_message?
    #   self.user.charge(policy.sms_to_send)
    #   self.update_attributes(status: :queue, status_message: STATUS_MESSAGES[:queue])
    #   Rabbitq::Client.new.publish 'message_worker', self.id
    # else
    #   self.update_attributes(status: :suspended, status_message: STATUS_MESSAGES[:suspended])
    # end
  end

  class NotPersisted < StandardError; end
end
