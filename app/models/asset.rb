# --- encoding: utf-8 ---
class Asset < ActiveRecord::Base
  validates :file, presence: true

  belongs_to :user

  mount_uploader :file, AssetsUploader

end
