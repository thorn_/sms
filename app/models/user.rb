# -*- encoding: utf-8 -*-
class User < ActiveRecord::Base
  store_accessor :settings, :language, :sender, :contract

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, # :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :generate_access_token

  has_many :contacts, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :groups, dependent: :destroy
  has_many :assets, dependent: :destroy

  after_create :create_main_group

  def create_main_group
    self.groups.create(name: 'All contacts', editable: false)
  end

  def main_group
    self.groups.find_by_editable(false)
  end

  # role model
  validates_presence_of :role

  def role_symbols
    user_role = self.role
    if user_role then [user_role.to_sym] else [:guest] end
  end

  def user_role
    roles = {"admin" => "Administrator", "user" => "User", "moderator" => "Moderator"}
    roles[self.role]
  end

  def charge(amount)
    raise NotEnoughSms if amount > self.sms_amount
    self.decrement!(:sms_amount, amount)
  end

  class NotEnoughSms < StandardError; end

  private

  def generate_access_token
    begin
      self.access_token = SecureRandom.hex
    end while self.class.exists?(access_token: access_token)
  end

end
