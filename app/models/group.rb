class Group < ActiveRecord::Base
  has_many :contact_groups, dependent: :destroy
  has_many :contacts, through: :contact_groups, dependent: :destroy

  belongs_to :user

  validates_presence_of :user_id, :name

  def clear
    if self.editable?
      self.contact_groups.delete_all
    else
      self.contacts.update_all(hidden: true)
    end
    true
  end
end
