class ContactMessage < ActiveRecord::Base

  enum status: [:queue, :success, :error]

  belongs_to :contact
  belongs_to :message

  attr_accessor :pagination

  def self.status(jid, status, status_message=nil)
    if contact_message = ContactMessage.find_by_jid(jid)
      contact_message.update_attributes(status: status, status_message: status_message)
    end
  end

end
