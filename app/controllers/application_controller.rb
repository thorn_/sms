# --- encoding: utf-8 ---
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  filter_access_to :all unless Rails.env == 'test'

  before_action :authenticate_user!
  before_action :set_locale
  before_action :set_gon_current_user

  protect_from_forgery

  def set_locale
    I18n.locale = get_locale()
  end

  def default_url_options(options={})
    logger.debug "default_url_options is passed options: #{options.inspect}\n"
    { locale: I18n.locale }
  end

  def get_locale
    if current_user && available_languages.include?(current_user.language)
      current_user.language
    elsif accept_lang = request.env['HTTP_ACCEPT_LANGUAGE']
      lang = accept_lang.scan(/^[a-z]{2}/).first
      lang if available_languages.include?(lang)
    else
      params[:locale] || :ru
    end
  end

  def available_languages
    ['ru', 'en']
  end

  def set_gon_current_user
    gon.current_user = {id: current_user.id, access_token: current_user.access_token, email: current_user.email, balance: current_user.sms_amount} if current_user
  end
end
