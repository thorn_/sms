# --- encoding: utf-8 ---
class Admin::UsersController < Admin::BaseController

  before_filter :find_user, only: [:edit, :update, :destroy]

  def index
    @users = User.order(:created_at)
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path, flash: {success: "User successfully removed."}
  end

  def edit
  end

  def new
    @user = User.new
  end

  def update
    parameters = user_params
    if user_params[:password].blank?
      parameters.delete(:password)
      parameters.delete(:password_confirmation)
    end
    @user.update_attributes(parameters)
    redirect_to admin_users_path, flash: { success: "User #{@user.email} successfully updated." }
  end

  def create
    parameters = user_params
    @user = User.new(parameters)
    if @user.save
      redirect_to admin_users_path, flash: { success: "User #{@user.email} successfully updated." }
    else
      render :new
    end
  end

  private
    def find_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :role, :sms_amount, :sender, :contract)
    end
end
