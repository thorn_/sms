# --- encoding: utf-8 ---
class Api::V1::ContactsController < Api::BaseController
  prepend_before_action :restrict_access
  def index
    per_page = 20
    @search = @user.contacts.opened.includes(:groups).order(:number).search(params[:q])
    @contacts = @search.result.page(params[:page]).per_page(per_page)
    prepare_pagination(@contacts, per_page)
    respond_to do |format|
      format.json
      format.xls do
        send_data create_spreadsheet(@search), type: "application/vnd.ms-excel"
      end
    end
  end

  def create
    @contact = @user.contacts.new(contact_params)
    if groups = @user.groups.where(id: params[:group_ids])
      @contact.groups = (@contact.groups + groups + [@user.main_group]).compact.uniq
    end
    unless @contact.save
      render json: {status: "error", errors: @contact.errors}.to_json, status: :unprocessable_entity
    end
  end

  def update
    @contact = @user.contacts.find(params[:id])
    groups = @user.groups.where(id: params[:group_ids])
    @contact.groups = (@contact.groups + groups).uniq
    unless @contact.update_attributes(contact_params)
      render json: {status: "error", errors: 'Invalid attributes'}, status: :unprocessable_entity
    end
  end

  def destroy
    @contact = @user.contacts.find(params[:id])
    group = @user.groups.find_by_id(params[:from_group])
    if group && group.editable?
      @contact.contact_groups.where(group_id: group.id).destroy_all
    else
      @contact.update_attributes!(hidden: true)
    end
  end

  def show
    @contact = @user.contacts.find(params[:id])
  end

  def add
    @contact = @user.contacts.new(simple_contact_params)
    @contact.groups = [@user.main_group]
    if @contact.save
      render json: {status: "ok"}
    else
      render json: {status: "error", errors: "contact is invald or already exists"}.to_json, status: :unprocessable_entity
    end
  end

  def del
    @contact = @user.contacts.find_by_number(simple_contact_params["number"])
    if @contact && @contact.update_attributes(hidden: true)
      render json: {status: "ok"}.to_json
    else
      render json: {status: "error", errors: "record not found"}, status: 404
    end
  end

  private

    def contact_params
      params.require(:contact).permit(:number, :name, :group_ids, :surname, :last_name)
    end

    def simple_contact_params
      params.permit(:number)
    end

    def create_spreadsheet(search)
      contacts = search.result
      spreadsheet = StringIO.new
      book = Spreadsheet::Workbook.new
      worksheet = book.create_worksheet name: "Contacts"
      worksheet.row(0).concat ["Phone number", "Last name", "First name", "Middle name"]
      search.result.each_with_index do |contact, i|
        worksheet.row(i + 1).concat([contact.number, contact.last_name, contact.name, contact.surname])
      end
      book.write spreadsheet
      spreadsheet.string
    end
end
