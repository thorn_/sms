# --- encoding: utf-8 ---
class Api::V1::GroupsController < Api::BaseController
  prepend_before_action :restrict_access
  before_action :find_group, only: [:show, :destroy, :update, :clear]

  def index
    @groups = @user.groups.order(created_at: :asc)
  end

  def show
  end

  def create
    @group = @user.groups.new(group_params)
    unless @group.save
      render json: @group.errors, status: :unprocessable_entity
    end
  end

  def destroy
    unless @group && @group.editable? && @group.destroy
      render json: {errors: "Error"}, status: :unprocessable_entity
    end
  end

  def update
    unless @group && @group.editable && @group.update_attributes(group_params)
      render json: {errors: "Error"}, status: :unprocessable_entity
    end
  end

  def clear
    unless @group && @group.clear
      render json: {errors: "Error"}, status: :unprocessable_entity
      return false
    end
  end

  private
  def group_params
    params.require(:group).permit(:name)
  end

  def find_group
    @group = @user.groups.find_by_id(params[:id])
  end
end
