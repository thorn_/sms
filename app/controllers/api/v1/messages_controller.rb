# --- encoding: utf-8 ---
class Api::V1::MessagesController < Api::BaseController
  prepend_before_action :restrict_access, except: :set_status

  def index
    per_page = 20
    @search = @user.messages.order(created_at: :desc).search(params[:q])
    @messages = @search.result.page(params[:page]).per_page(per_page)
    prepare_pagination(@messages, per_page)
  end

  def show
    @message = @user.messages.find_by_id(params[:id])
    per_page = 20
    if @message
      @search = @message.contact_messages.order(created_at: :desc).search(params[:q])
      @contact_messages = @search.result.page(params[:page]).per_page(per_page)
      prepare_pagination(@contact_messages, per_page)
    else
      render json: {status: "error", errors: "Message not found"}.to_json, status: :not_found
    end
  end

  def create
    @message = MessageCreator.create(params, @user)
    unless @message
      render json: {status: "error", errors: "Error creating message"}.to_json, status: :unprocessable_entity
    end
  end

  def set_status
    data = params[:data] || {}
    data.each_pair do |value, status|
      result = status.split("\n")
      if result && result.any? && result[0] == 'sms_status'
        sid = result[1]
        status = SmsRuApi.get_status_from(result[2])
        message = ContactMessage.find_by_service_id(sid)
        message.update_attributes(status: status[:status], status_message: status[:message]) if message
      end
    end
    render text: "100"
  end

  def send_message
    @message = @user.messages.new(simple_message_params)
    if simple_message_params["test"] == "true"
      if @message.valid?
        render json: {status: "ok", message: {id: "0", text: @message.text} }.to_json
      else
        render json: {status: "error", errors: "Error creating message"}.to_json, status: :unprocessable_entity
      end
    elsif @user.contacts.opened.count > 0
      if (@message.contacts = @user.contacts) && @message.save
        @message.contact_messages.each do |contact_message|
          raise "There is no sidekiq now"
        end
        render json: render_to_string( template: '/api/v1/messages/send_message.json.jbuilder', locals: { message: @message})
      else
        render json: {status: "error", errors: "Error creating message"}.to_json, status: :unprocessable_entity
      end
    else
      render json: {status: "error", errors: "you have no contacts to send message to"}.to_json, status: 404
    end
  end

  def destroy
    @message = @user.messages.find(params[:id])
    @message.destroy
    render json: 'ok'
  end

  def restart
    @message = @user.messages.find_by_id(params[:id])
    if @message
      @message.start
    else
      render json: {status: "error", errors: "Message not found"}.to_json, status: :not_found
    end
  end

  private

    def simple_message_params
      params.permit(:text, :url, :test)
    end
end
