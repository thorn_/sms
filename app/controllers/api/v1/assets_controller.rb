# --- encoding: utf-8 ---
class Api::V1::AssetsController < Api::BaseController
  prepend_before_action :restrict_access
  def index
    @assets = @user.assets
  end

  def create
    @user.assets.destroy_all
    @asset = @user.assets.new(asset_params)
    if @asset.save
      # ContactWorker.perform_async(@asset.file.path, @user.id, params[:group_id])
      Rabbitq::Client.new.publish('contacts', {file_path: @asset.file.path, user_id: @user.id, group_id: params[:group_id]}.to_json, {routing_key: 'contacts.import'})
    else
      render json: {errors: @asset.errors.to_json}, status: :unprocessable_entity
    end
  end

  def destroy
    @asset = @user.assets.find(params[:id])
    if @asset
      @asset.destroy
    end
    render nothing: true
  end

  private
    def asset_params
      params.require(:asset).permit(:file)
    end
end
