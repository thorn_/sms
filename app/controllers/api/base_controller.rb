# --- encoding: utf-8 ---
module Api
  class BaseController < ActionController::Base
    skip_before_action :authenticate_user!
    skip_before_action :verify_authenticity_token

    private

    def restrict_access
      @user = User.find_by_access_token(params[:access_token])
      head :unauthorized unless @user
    end

    def prepare_pagination(things, per_page)
      things.each do |thing|
        thing.pagination = {
          totalPages: things.total_pages,
          totalEntries: things.total_entries,
          itemsPerPage: per_page,
          page: things.current_page
        }
      end
    end
  end
end
